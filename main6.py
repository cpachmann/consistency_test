# auditing_functions/main.py
import os
import shutil
import stat
import logging
import sys
import signal
from .functions.file_audit import FileAuditor
from .functions.calculate_overall_scores import calculate_overall_scores
from .sub_functions.status_update import update_audit_status
from .functions.audit_scoring import audit_scoring, insert_or_update_audit_results
from .functions.contextualizer import Contextualizer
from .functions.ai_contextualizer import AIContextualizer
from .functions.audit_batches_consolidation import AuditBatchesConsolidation
from typedb.driver import *


logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

user = "cp"
password = "ahlli230913adsk"


def force_remove_directory(dir_path):
    try:
        shutil.rmtree(dir_path, onerror=lambda func, path, exc_info: os.chmod(path, stat.S_IWRITE))
        logging.info(f"Successfully cleaned up the cache directory: {dir_path}")
    except Exception as e:
        logging.error(f"Error cleaning up the cache directory: {e}")

def fetch_audit_details(audit_uuid):
    # Query TypeDB for audit_result_uuid, cache_dir, or any other required data
    audit_result_uuid = None
    cache_dir = None

    with TypeDB.core_driver(TypeDB.DEFAULT_ADDRESS) as driver:
        with driver.session("CodeDD_test_1", SessionType.DATA) as session:
            with session.transaction(TransactionType.READ) as tx:
                try:
                    # Fetch audit_result_uuid
                    query_audit_result = f'''
                        match
                            $audit isa audit, has audit_uuid "{audit_uuid}";
                            $audit_result isa audit_result, has audit_result_uuid $result_uuid;
                            (audit_initiator: $audit, audit_resultant: $audit_result) isa audit_outcome;
                        get $result_uuid;
                    '''
                    answers_audit_result = tx.query.get(query_audit_result)
                    for answer in answers_audit_result:
                        audit_result_uuid = answer.get("result_uuid").get_value()

                    # Fetch cache_dir (root_path)
                    query_root_path = f'''
                        match
                            $audit isa audit, has audit_uuid "{audit_uuid}";
                            (audit_association: $audit, associated_directory: $dir) isa audit_directory;
                            $dir isa root_directory, has root_path $root_path;
                        get $root_path;
                    '''
                    answers_root_path = tx.query.get(query_root_path)
                    for answer in answers_root_path:
                        cache_dir = answer.get("root_path").get_value()

                except Exception as e:
                    print(f"An error occurred during transaction for audit details: {e}")

    return audit_result_uuid, cache_dir

def main_process_flow(audit_uuid): 

    try:
        # Query TypeDB for audit_result_uuid, cache_dir, or any other required data
        audit_result_uuid, cache_dir = fetch_audit_details(audit_uuid)

        # Step 5: Auditing files - we handle previous steps in audit_scope_selector.py
        print("===== Entering Step 5: Auditing Files =====")
        logging.info("Starting the file auditing process...")
        
        file_paths = FileAuditor.fetch_files_from_repo(audit_uuid) 
        logging.info(f"File paths fetched for auditing: {file_paths}")

        if not file_paths:
            logging.warning("No suitable file paths found for auditing.")
        else:
            try:
                if not FileAuditor.ai_auditing(file_paths, audit_uuid):
                    raise Exception("File auditing failed.")
                logging.info("File auditing completed successfully.")
                update_audit_status(audit_uuid, "File auditing completed")
            except Exception as e:
                logging.error(f"Error during auditing: {e}")
                raise
        logging.error(f"Starting audit_scoring for file paths.")
        average_scores = audit_scoring(file_paths)
        insert_or_update_audit_results(audit_result_uuid, average_scores)
        
        # Calculate Overall Scores
        try:
            logging.info("Calculating overall scores...")
            calculate_overall_scores(audit_result_uuid)
            logging.info("Overall scores calculation completed successfully.")
        except Exception as e:
            logging.error(f"Error during overall score calculation: {e}")
            raise

        update_audit_status(audit_uuid, "Scores calculated")


        # Step 6: Contextualizing Files
        success = True  # Initialize success as True at the start of this block
        try:
            unique_domains = Contextualizer.retrieve_unique_domains(audit_uuid)
            domain_file_paths = Contextualizer.retrieve_file_paths_by_domain(audit_uuid, unique_domains)
            ai_contextualizer = AIContextualizer(audit_uuid, audit_result_uuid)

            for domain, paths in domain_file_paths.items():
                file_attributes = Contextualizer.fetch_file_attributes(paths)
                batches = Contextualizer.batch_files(file_attributes)

                for batch_data in batches:  # batch_data is a tuple of (batch, batch_uuid)
                    batch, batch_uuid = batch_data  # Unpack the tuple
                    if not ai_contextualizer.process_batch(batch, batch_uuid):
                        success = False
                        logging.warning(f"Contextualization failed for domain: {domain}")
                        break
                if not success:
                    break

            if not success:
                raise Exception("Contextualization process failed.")
        except Exception as e:
            logging.error(f"Error in contextualizing files: {e}")
            success = False

        if success:
            update_audit_status(audit_uuid, "Contextualization completed")
        else:
            update_audit_status(audit_uuid, "Contextualization failed")
            
        # Step 7: Audit Consolidation and Analysis
        print("===== Entering Step 7: Audit Consolidation and Analysis =====")
        try:
            consolidation = AuditBatchesConsolidation(audit_result_uuid)

            # Retrieve batched data
            batch_data = consolidation.retrieve_batched_data()
            if not batch_data:
                raise Exception("Failed to retrieve batched data.")

            # Perform audit batches and write to TypeDB
            summary_data = consolidation.audit_batches(batch_data)
            if summary_data:
                consolidation.write_consolidation_to_typedb(summary_data)

            logging.info(f"Audit completed successfully for {audit_uuid} and audit_result {audit_result_uuid}.")
            update_audit_status(audit_uuid, "Audit completed")

        except Exception as e:
            logging.error(f"Error during Audit Consolidation and Analysis: {e}")
            update_audit_status(audit_uuid, "Audit failed")
            success = False

    except Exception as e:
        logging.error(f"Error in main_process_flow: {e}")
        if audit_uuid:
            update_audit_status(audit_uuid, "Process failed due to error")
        success = False
    finally:
        if cache_dir and os.path.exists(cache_dir):
            force_remove_directory(cache_dir)
    
    return success

def get_git_url_demo():
    return "https://gitlab.com/cpachmann/consistency_test"


def exit_gracefully(signum, frame):
    logging.info("Graceful exit initiated.")
    sys.exit(0)

if __name__ == '__main__':
    signal.signal(signal.SIGINT, exit_gracefully)
    if len(sys.argv) > 1:
        git_repo_path = sys.argv[1]
        main_process_flow(git_repo_path)
    else:
        logging.error("No Git repository path provided as argument.")
